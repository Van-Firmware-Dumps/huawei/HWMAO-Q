# Copyright (c) 2018-2020, The Linux Foundation. All rights reserved.
# Copyright (c) 2022 Qualcomm Innovation Center, Inc. All rights reserved.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
#     * Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#     * Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#     * Neither the name of The Linux Foundation nor the names of its
#       contributors may be used to endorse or promote products derived
#       from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED "AS IS" AND ANY EXPRESS OR IMPLIED
# WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
# MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT
# ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS
# BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR
# BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE
# OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN
# IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
#

on property:ro.vendor.user_hota_update=1
    write /sys/devices/system/cpu/cpu0/cpufreq/scaling_max_freq 940800
    write /sys/devices/system/cpu/cpu4/cpufreq/scaling_max_freq 1344000
    write /dev/kmsg "first boot, user_hota_update = 1.  limit cpufreq(JLN)."

on property:ro.vendor.user_hota_update=1 && property:odm.thermal.hw_mc.hardware.upgrade_thermal_control=1
    write /sys/class/thermal/thermal_zone0/mode 1
    limit_soc_thermal_temperature /sys/class/thermal/thermal_zone0/trip_point_1_temp
    write /dev/kmsg "first boot, user_hota_update = 1. change ipa control temp."

on property:ro.vendor.user_hota_update=1 && property:odm.thermal.hw_mc.hardware.upgrade_thermal_control=1 && property:odm.thermal.hw_mc.hardware.upgrade_thermal_disable=1
    restore_soc_thermal_temperature /sys/class/thermal/thermal_zone0/trip_point_1_temp
    write /dev/kmsg "first boot, user_hota_update = 1. restore ipa control temp."

on early-init
    exec u:r:vendor_modprobe:s0 -- /vendor/bin/modprobe -a -d /vendor/lib/modules audio_q6_pdr audio_q6_notifier audio_snd_event audio_apr audio_adsp_loader audio_q6 audio_native audio_usf audio_pinctrl_lpi audio_swr audio_swr_ctrl audio_platform audio_stub audio_wcd_core audio_wcd9xxx audio_aw882xx audio_wsa881x_analog audio_bolero_cdc audio_va_macro audio_rx_macro audio_tx_macro audio_mbhc audio_wcd937x audio_wcd937x_slave audio_pm2250_spmi audio_rouleur audio_rouleur_slave audio_machine_bengal
    write /proc/sys/kernel/sched_boost 1
    exec u:r:vendor_qti_init_shell:s0 -- /vendor/bin/init.qti.early_init.sh
    setprop ro.soc.model ${ro.vendor.qti.soc_model}
    write /proc/boottime "INIT: on init start"

on init
    write /dev/stune/foreground/schedtune.sched_boost_no_override 1
    write /dev/stune/top-app/schedtune.sched_boost_no_override 1
    write /dev/stune/schedtune.colocate 0
    write /dev/stune/background/schedtune.colocate 0
    write /dev/stune/system-background/schedtune.colocate 0
    write /dev/stune/foreground/schedtune.colocate 0
    write /dev/stune/top-app/schedtune.colocate 1
    #Moving to init as this is needed for qseecomd
    wait /dev/block/platform/soc/${ro.boot.bootdevice}
    symlink /dev/block/platform/soc/${ro.boot.bootdevice} /dev/block/bootdevice
    start vendor.qseecomd
    start keymaster-4-0

on early-fs
    start vold

on fs
    start hwservicemanager
    write /proc/boottime "INIT:eMMC:Mount_START"
    mount_all --early
    chown root system /mnt/vendor/persist
    chmod 0771 /mnt/vendor/persist
    restorecon_recursive /mnt/vendor/persist
    write /proc/boottime "INIT:eMMC:Mount_END"
    mkdir /mnt/vendor/persist/data 0700 system system
    mkdir /mnt/vendor/persist/audio 0770 audio audio

on post-fs
    # set RLIMIT_MEMLOCK to 64MB
    setrlimit 8 67108864 67108864

on late-fs
    wait_for_prop hwservicemanager.ready true
    mount_all --late

on post-fs-data
    mkdir /vendor/data/tombstones 0771 system system

on early-boot
    start vendor.sensors

on boot
    write /dev/cpuset/audio-app/cpus 1-2
#USB controller configuration
    setprop vendor.usb.rndis.func.name "gsi"
    setprop vendor.usb.rmnet.func.name "gsi"
    setprop vendor.usb.rmnet.inst.name "rmnet"
    setprop vendor.usb.dpl.inst.name "dpl"
    setprop vendor.usb.qdss.inst.name "qdss"
    setprop vendor.usb.controller 4e00000.dwc3
#touch boost
    setprop sys.huawei.touch_boost 1

#Load WLAN driver
    insmod /vendor/lib/modules/qca_cld3_wlan.ko

on boot && property:persist.vendor.usb.controller.default=*
    setprop vendor.usb.controller ${persist.vendor.usb.controller.default}

on property:vendor.usb.controller=*
    setprop sys.usb.controller ${vendor.usb.controller}

on charger
    start vendor.power_off_alarm
    setprop vendor.usb.controller 4e00000.dwc3
    setprop sys.usb.configfs 1

#pd-mapper
service vendor.pd_mapper /vendor/bin/pd-mapper
    class core
    user system
    group system

#Peripheral manager
service vendor.per_mgr /vendor/bin/pm-service
    class core
    user system
    group system
    ioprio rt 4

service vendor.per_proxy /vendor/bin/pm-proxy
    class core
    user system
    group system
    disabled

service vendor.mdm_helper /vendor/bin/mdm_helper
    class core
    group system wakelock
    disabled

service vendor.mdm_launcher /vendor/bin/sh /vendor/bin/init.mdm.sh
    class core
    oneshot

on property:init.svc.vendor.per_mgr=running
    start vendor.per_proxy

on property:sys.shutdown.requested=*
    stop vendor.per_proxy

on property:vold.decrypt=trigger_restart_framework
   start vendor.cnss_diag

service vendor.cnss_diag /system/vendor/bin/cnss_diag -q -f -t HELIUM
   class main
   user system
   group system wifi inet sdcard_rw media_rw diag
   oneshot

service dcvs-sh /vendor/bin/init.qti.dcvs.sh
    class late_start
    user root
    group root system
    oneshot

on property:vendor.dcvs.prop=1
   start dcvs-sh

on boot
    chmod 0664 /dev/fingerprint
    chown system system dev/fingerprint
    chmod 0664 /sys/devices/platform/fingerprint/result
    chmod 0664 /sys/devices/platform/fingerprint/fingerprint_chip_info
    chmod 0664 /sys/devices/platform/fingerprint/irq
    chmod 0664 /sys/devices/platform/fingerprint/read_image_flag
    chmod 0664 /sys/devices/platform/fingerprint/snr
    chmod 0664 /sys/devices/platform/fingerprint/nav
    chmod 0664 /sys/devices/platform/fingerprint/module_id
    chmod 0664 /sys/devices/platform/fingerprint/module_id_ud
    chmod 0664 /sys/devices/platform/fingerprint/ud_fingerprint_chip_info
    chmod 0664 /sys/devices/platform/fingerprint/low_temperature
    chown system system /sys/devices/platform/fingerprint/result
    chown system system /sys/devices/platform/fingerprint/fingerprint_chip_info
    chown system system /sys/devices/platform/fingerprint/irq
    chown system system /sys/devices/platform/fingerprint/read_image_flag
    chown system system /sys/devices/platform/fingerprint/snr
    chown system system /sys/devices/platform/fingerprint/nav
    chown system system /sys/devices/platform/fingerprint/module_id
    chown system system /sys/devices/platform/fingerprint/module_id_ud
    chown system system /sys/devices/platform/fingerprint/ud_fingerprint_chip_info
    chown system system /sys/devices/platform/fingerprint/low_temperature
    # modify the permission of group stune to system
    chown system system /dev/stune/background/cgroup.procs
    chown system system /dev/stune/boost/cgroup.procs
    chown system system /dev/stune/foreground/cgroup.procs
    chown system system /dev/stune/graphic/cgroup.procs
    chown system system /dev/stune/key-background/cgroup.procs
    chown system system /dev/stune/top-app/cgroup.procs
    chown system system /dev/stune/vip/cgroup.procs
    chown system system /dev/stune/cgroup.procs
    chmod 0664 /dev/stune/background/cgroup.procs
    chmod 0664 /dev/stune/boost/cgroup.procs
    chmod 0664 /dev/stune/foreground/cgroup.procs
    chmod 0664 /dev/stune/graphic/cgroup.procs
    chmod 0664 /dev/stune/key-background/cgroup.procs
    chmod 0664 /dev/stune/top-app/cgroup.procs
    chmod 0664 /dev/stune/vip/cgroup.procs
    chmod 0664 /dev/stune/cgroup.procs
